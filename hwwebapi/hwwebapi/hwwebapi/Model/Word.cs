﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace hwwebapi.Model
{
    public partial class Word
    {

        [JsonProperty("type")]
        public string type { get; set; }

        [JsonProperty("definition")]
        public string definition { get; set; }

        [JsonProperty("example")]
        public string example { get; set; }
    }

    public partial class Word

    {
        public static Word FromJson(string json) => JsonConvert.DeserializeObject<Word>(json, hwwebapi.Model.Converter.Settings);
    }


    public static class Serialize
    {
        public static string ToJson(this Word self) => JsonConvert.SerializeObject(self, hwwebapi.Model.Converter.Settings);

    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
