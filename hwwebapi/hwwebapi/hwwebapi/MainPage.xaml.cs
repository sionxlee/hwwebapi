﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using hwwebapi.Model;
using Xamarin.Forms;

namespace hwwebapi
{
	 public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            
        }

        public bool checkInternet()
        {
            if (Plugin.Connectivity.CrossConnectivity.Current.IsConnected == true)
                return true;
            return false;
        }


        async void button_Clicked(object sender, EventArgs e)
        {
                if (checkInternet())
                {
                HttpClient client = new HttpClient();
                
                var w = input.Text;
                var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" +$"{w}"));

                var request = new HttpRequestMessage(HttpMethod.Get,uri);
                request.Headers.Add("Application", "application / json");


                HttpResponseMessage response = await client.SendAsync(request);
                Word worddata = null;
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    worddata = Word.FromJson(content);
                    WordDetail1.Text = $"{w} ({worddata.type})";
                    WordDetail2.Text = $"{worddata.definition}";
                    WordDetail3.Text = $"Example: {worddata.example}";
                }
                
            }
            else
            {
                await DisplayAlert("Warning", "No Internet Connection", "OK");
            }
            
        }
    }
}
